/**
  ******************************************************************************
  * @file    analyze.c
  * @author  Tobias Vogel @ OBLAMATIK
  * @brief   This module implements all sound relevant analyzing function.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 Oblamatik AG.
  * All rights reserved.</center></h2>
  *
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "analyze.h"
//#include "math.h"
//
#include "arm_math.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
#define AUDIO_FREQUENCY            BSP_AUDIO_FREQUENCY_16K
#define AUDIO_IN_PDM_BUFFER_SIZE  (uint32_t)(128*AUDIO_FREQUENCY/16000*2)
/* Size of the recorder buffer */
#define RECORD_BUFFER_SIZE        (4096*16) // Size of Record Buffer = 65'536, 1 / 2 used, 1 / 4 channel left (16'384) => ~1s
#define FFT_SIZE    256U
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint8_t ui8UpdateFlag = 0;
uint8_t ui8ReadyForFFT = 0;
uint32_t ui32RecordBufferChannelLeft = 0;
uint32_t ui32SoundBuffer = 0;
uint32_t ui32SoundBufferBlock = 0;

/* Private function prototypes -----------------------------------------------*/
void FillFFT_InBuffer(float *RecBuf, float * pfInBuf);
void Display_FFT(float *fft_mag);

/* Private functions ---------------------------------------------------------*/
extern void CalculateFFT(float * pfFFT_In)
{
  arm_rfft_fast_instance_f32 fft_inst;
  float fft_out[FFT_SIZE];
  float fft_mag[FFT_SIZE>>1];

  /* Initialize ARM FFT instance with num points */
  arm_rfft_fast_init_f32(&fft_inst, FFT_SIZE);

  /* Perform forward direction 32-bit FFT */
  arm_rfft_fast_f32(&fft_inst, pfFFT_In, fft_out, 0);

  /* Calculate magnitude (buffer size is half because real + imag parts are merged) */
  arm_cmplx_mag_f32(fft_out, fft_mag, FFT_SIZE >> 1);
  
  Display_FFT(fft_mag);
}

void Display_Wave(void)
{
  float f32SinVal;
  static float fStep = 0.0;
  
  /* Clear the LCD */
  UTIL_LCD_Clear(UTIL_LCD_COLOR_WHITE);

  /* Draw horizontal axis */
  UTIL_LCD_DrawRect(1, 240, 800, 2, UTIL_LCD_COLOR_BLUE);
    
  /* Draw plot */
  for (uint32_t i = 0; i < 800; i++)
  {
	f32SinVal = 240.0 * (1.0 - (sin((float)i * 2.0 * M_PI / 800.0 + fStep )));
    UTIL_LCD_DrawRect(i, (uint32_t)f32SinVal, 1, 1, UTIL_LCD_COLOR_BLUE);
  }
  fStep += 0.01;
  if (fStep == 1.0)
  {
	  fStep = 0.0;
  }
}

void Display_Sound(uint16_t *RecBuf, uint32_t ui32CurPos)
{
  float fY_Val;
  uint32_t u32CorPos;

  if (ui32CurPos == 0)
  {
    u32CorPos = (RECORD_BUFFER_SIZE * 2) - 1600;
  }
  else
  {
    u32CorPos = ui32CurPos - 1600;
  }
  
  /* Clear the LCD */
  UTIL_LCD_Clear(UTIL_LCD_COLOR_WHITE);

  /* Draw horizontal axis */
  UTIL_LCD_DrawRect(1, 240, 800, 2, UTIL_LCD_COLOR_BLUE);

  /* Draw plot */
  for (uint32_t i = 1; i <= 800; i+=2)
  {
    fY_Val = 480.0 * (1.0 - (float)RecBuf[u32CorPos + i * 2] / 65535.0);
    UTIL_LCD_DrawRect(i, (uint32_t)fY_Val, 1, 1, UTIL_LCD_COLOR_BLUE);
  }
}

void Display_FFT(float *fft_mag)
{
  uint8_t i;
  float max_val;
  uint32_t max_idx;
  uint32_t box_height;
  uint8_t box_width = (776 / (FFT_SIZE >> 1));

  UTIL_LCD_FillRect(12, 102, 776, 366, UTIL_LCD_COLOR_LIGHTGREEN);

  /* Draw horizontal axis */
  UTIL_LCD_FillRect(12, 283, 776, 2, UTIL_LCD_COLOR_BLUE);

  /* Use max value in the results for scale */
  arm_max_f32(fft_mag, FFT_SIZE >> 1, &max_val, &max_idx);

  /* Draw frequency bins */
  for (i = 0; i < (FFT_SIZE >> 1); i++)
  {
	box_height = (uint32_t)(183.0 * (fft_mag[i] / max_val));
	UTIL_LCD_FillRect(12 + box_width * i, 283 - box_height, box_width, box_height, UTIL_LCD_COLOR_LIGHTRED);
  }
}

/************************ (C) Oblamatik AG *****END OF FILE****/
