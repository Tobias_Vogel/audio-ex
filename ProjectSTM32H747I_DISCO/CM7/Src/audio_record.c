/**
  ******************************************************************************
  * @file    BSP/CM7/Src/audio_record.c
  * @author  MCD Application Team
  * @brief   This example describes how to use DFSDM HAL API to realize
  *          audio recording.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "analyze.h"
#include "audioringbuffer.h"
#include "app_x-cube-ai.h"

/** @addtogroup BSP_Examples
  * @{
  */

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
/* Audio frequency */
extern AUDIO_ErrorTypeDef AUDIO_Start(uint32_t audio_start_address, uint32_t audio_file_size);
#define AUDIO_FREQUENCY            16000U /* AUDIO_FREQUENCY_16K; */
#define AUDIO_IN_PDM_BUFFER_SIZE  (uint32_t)(128 * AUDIO_FREQUENCY / 16000 * 2)
#define AUDIO_NB_BLOCKS    ((uint32_t)4)
#define AUDIO_BLOCK_SIZE   ((uint32_t)0xFFFE)
/* Size of the recorder buffer */
#define RECORD_BUFFER_SIZE        4096
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Define record Buf at D3SRAM @0x38000000 since the BDMA for SAI4 use only this memory */
ALIGN_32BYTES (uint16_t recordPDMBuf[AUDIO_IN_PDM_BUFFER_SIZE]) __attribute__((section(".RAM_D3")));
ALIGN_32BYTES (uint16_t  RecPlayback[2*RECORD_BUFFER_SIZE]);
ALIGN_32BYTES (RingBuffer sAudioRingBuffer);
ALIGN_32BYTES (float fFFT_Buffer[AUDIO_RING_BUFFER_LENGTH]);
uint32_t VolumeLevelOutput = 20;
uint32_t VolumeLevelInput = 80;
uint32_t  InState = 0;
uint32_t  OutState = 0;
BSP_AUDIO_Init_t  AudioInInit;
BSP_AUDIO_Init_t  AudioOutInit;
/* Pointer to record_data */
uint32_t playbackPtr = 0;
uint32_t AudioBufferOffset;
uint8_t ui8ProcessAudioRingbuffer = 0;
uint8_t uc8BlinkLED = 0;
/* Private function prototypes -----------------------------------------------*/

typedef enum {
  BUFFER_OFFSET_NONE = 0,
  BUFFER_OFFSET_HALF,
  BUFFER_OFFSET_FULL,
}BUFFER_StateTypeDef;
/* Private functions ---------------------------------------------------------*/

/**
  * @brief Test Audio record.
  *   The main objective of this test is to check the hardware connection of the
  *   Audio peripheral.
  * @param  None
  * @retval None
*/
void AudioRecord_demo(void)
{
  uint32_t channel_nbr = 2;

  uint32_t x_size, y_size;

  BSP_LCD_GetXSize(0, &x_size);
  BSP_LCD_GetYSize(0, &y_size);

  /* Clear the LCD */
  UTIL_LCD_Clear(UTIL_LCD_COLOR_WHITE);
  /* Set Audio Demo description */
  UTIL_LCD_FillRect(0, 0, x_size, 90, UTIL_LCD_COLOR_BLUE);
  UTIL_LCD_SetTextColor(UTIL_LCD_COLOR_WHITE);
  UTIL_LCD_SetBackColor(UTIL_LCD_COLOR_BLUE);
  UTIL_LCD_SetFont(&Font24);
  UTIL_LCD_DisplayStringAt(0, 0, (uint8_t *)"DETECTION OF THE BUSINESS SIZE (BIG or small)", CENTER_MODE);
  UTIL_LCD_SetFont(&Font16);
  UTIL_LCD_DisplayStringAt(0, 24, (uint8_t *)"Make sure the microphone is attached to the toilet!", CENTER_MODE);
  UTIL_LCD_DisplayStringAt(0, 40,  (uint8_t *)"Press blue button for stop detection.", CENTER_MODE);
  /* Set the LCD Text Color */
  UTIL_LCD_DrawRect(10, 100, x_size - 20, y_size - 110, UTIL_LCD_COLOR_BLUE);
  UTIL_LCD_DrawRect(11, 101, x_size - 22, y_size - 112, UTIL_LCD_COLOR_BLUE);

  AudioOutInit.Device = AUDIO_OUT_DEVICE_AUTO;
  AudioOutInit.ChannelsNbr = channel_nbr;
  AudioOutInit.SampleRate = AUDIO_FREQUENCY;
  AudioOutInit.BitsPerSample = AUDIO_RESOLUTION_16B;
  AudioOutInit.Volume = VolumeLevelOutput;

  AudioInInit.Device = AUDIO_IN_DEVICE_DIGITAL_MIC;
  AudioInInit.ChannelsNbr = channel_nbr;
  AudioInInit.SampleRate = AUDIO_FREQUENCY;
  AudioInInit.BitsPerSample = AUDIO_RESOLUTION_16B;
  AudioInInit.Volume = VolumeLevelInput;

  BSP_JOY_Init(JOY1, JOY_MODE_GPIO, JOY_ALL);

  RingBuffer_Init(&sAudioRingBuffer);

  /* Initialize Audio Recorder with 2 channels to be used */
  BSP_AUDIO_IN_Init(1, &AudioInInit);
  BSP_AUDIO_IN_GetState(1, &InState);

  BSP_AUDIO_OUT_Init(0, &AudioOutInit);

  BSP_AUDIO_OUT_SetDevice(0, AUDIO_OUT_DEVICE_HEADPHONE);

  /* Start Recording */
  BSP_AUDIO_IN_RecordPDM(1, (uint8_t*)&recordPDMBuf, 2*AUDIO_IN_PDM_BUFFER_SIZE);

  /* Play the recorded buffer */
  BSP_AUDIO_OUT_Play(0, (uint8_t*)&RecPlayback[0], 2*RECORD_BUFFER_SIZE);

  //TBD  MX_X_CUBE_AI_Init();

  while (1)
  {
	  if (RingBuffer_GetDataLength(&sAudioRingBuffer) >= 17600)
	  {
		if (uc8BlinkLED)
		{
			uc8BlinkLED = 0;
			BSP_LED_Off(LED3); // red LED off
		}
		else
		{
			uc8BlinkLED = 1;
			BSP_LED_On(LED3); // red LED on
		}

		BSP_LED_On(LED4); // blue LED on

		// 16000 measurements to consider for FFT window (according to 1 s)
		RingBufferReadForFFT(&sAudioRingBuffer, fFFT_Buffer, 16000);

		// Execute FFT
		CalculateFFT(&fFFT_Buffer[0]);

		// Only 1600 measurements to remove (according to 100 ms)
		RingBufferReadTrash(&sAudioRingBuffer, fFFT_Buffer, 1600);

		//TBD8MX_X_CUBE_AI_Process();
		BSP_LED_Off(LED4); // blue LED off
	}

    if (CheckForUserInput() > 0)
    {
      ButtonState = 0;
      BSP_AUDIO_OUT_Stop(0);
      BSP_AUDIO_OUT_DeInit(0);
      BSP_AUDIO_IN_Stop(1);
      BSP_AUDIO_IN_DeInit(1);
      return;
    }
  }
}

/**
  * @brief Calculates the remaining file size and new position of the pointer.
  * @param  None
  * @retval None
  */
void  BSP_AUDIO_IN_TransferComplete_CallBack(uint32_t Instance)
{
	if(Instance == 1U)
	{

#ifdef GENERATE_SINUS_WAVE_ENABLED
		float fSampleFrequency = 16000.0;
		float fT_Time = 1.0 / fSampleFrequency;
		for (uint32_t ui32NewData = playbackPtr; ui32NewData < (playbackPtr + AUDIO_IN_PDM_BUFFER_SIZE/4/2); ui32NewData++)
		{
			float ft = fT_Time * ui32NewData;
			RecPlayback[ui32NewData] = (uint16_t)(65535.0 * sin(2 * M_PI * 1000 * ft) + 0.5 * sin(2 * M_PI * 15000 * ft));
		}
#else
		/* Invalidate Data Cache to get the updated content of the SRAM*/
		SCB_InvalidateDCache_by_Addr((uint32_t *)&recordPDMBuf[AUDIO_IN_PDM_BUFFER_SIZE/2], AUDIO_IN_PDM_BUFFER_SIZE*2);

		BSP_AUDIO_IN_PDMToPCM(Instance, (uint16_t*)&recordPDMBuf[AUDIO_IN_PDM_BUFFER_SIZE/2], &RecPlayback[playbackPtr]);

		/* Clean Data Cache to update the content of the SRAM */
		SCB_CleanDCache_by_Addr((uint32_t*)&RecPlayback[playbackPtr], AUDIO_IN_PDM_BUFFER_SIZE/4);
#endif

		if (RingBuffer_Write(&sAudioRingBuffer, &RecPlayback[playbackPtr], AUDIO_IN_PDM_BUFFER_SIZE/4/2) != RING_BUFFER_OK)
		{
			while (1);
		}

		playbackPtr += AUDIO_IN_PDM_BUFFER_SIZE/4/2;
		if(playbackPtr >= RECORD_BUFFER_SIZE)
		{
			playbackPtr = 0;
		}
	}
	else
	{
		AudioBufferOffset = BUFFER_OFFSET_FULL;
	}
}

/**
  * @brief  Manages the DMA Half Transfer complete interrupt.
  * @param  None
  * @retval None
  */
void BSP_AUDIO_IN_HalfTransfer_CallBack(uint32_t Instance)
{
	if(Instance == 1U)
	{

#ifdef GENERATE_SINUS_WAVE_ENABLED
		float fSampleFrequency = 16000.0;
		float fT_Time = 1.0 / fSampleFrequency;
		for (uint32_t ui32NewData = playbackPtr; ui32NewData < (playbackPtr + AUDIO_IN_PDM_BUFFER_SIZE/4/2); ui32NewData++)
		{
			float ft = fT_Time * ui32NewData;
			RecPlayback[ui32NewData] = (uint16_t)(65535.0 * sin(2 * M_PI * 1000 * ft) + 0.5 * sin(2 * M_PI * 15000 * ft));
		}
#else
		/* Invalidate Data Cache to get the updated content of the SRAM*/
		SCB_InvalidateDCache_by_Addr((uint32_t *)&recordPDMBuf[0], AUDIO_IN_PDM_BUFFER_SIZE*2);

		BSP_AUDIO_IN_PDMToPCM(Instance, (uint16_t*)&recordPDMBuf[0], &RecPlayback[playbackPtr]);

		/* Clean Data Cache to update the content of the SRAM */
		SCB_CleanDCache_by_Addr((uint32_t*)&RecPlayback[playbackPtr], AUDIO_IN_PDM_BUFFER_SIZE/4);
#endif

		if (RingBuffer_Write(&sAudioRingBuffer, &RecPlayback[playbackPtr], AUDIO_IN_PDM_BUFFER_SIZE/4/2) != RING_BUFFER_OK)
		{
			while (1);
		}

		playbackPtr += AUDIO_IN_PDM_BUFFER_SIZE/4/2;
		if(playbackPtr >= RECORD_BUFFER_SIZE)
		{
			playbackPtr = 0;
		}
	}
	else
	{
		AudioBufferOffset = BUFFER_OFFSET_HALF;
	}
}

/**
  * @brief  Audio IN Error callback function
  * @param  None
  * @retval None
  */
void BSP_AUDIO_IN_Error_CallBack(uint32_t Instance)
{
  /* Stop the program with an infinite loop */
  Error_Handler();
}
/**
  * @}
  */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
