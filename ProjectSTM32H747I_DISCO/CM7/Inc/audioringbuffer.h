#ifndef AUDIO_RING_BUFFER_H__
#define AUDIO_RING_BUFFER_H__

#include <stdint.h>


#define AUDIO_RING_BUFFER_LENGTH (18000U) // 1 s audio ring buffer => 16000

typedef struct {
	uint16_t  buf[AUDIO_RING_BUFFER_LENGTH];
	uint16_t head, tail;
} RingBuffer;

typedef enum {
	RING_BUFFER_OK = 0x0,
	RING_BUFFER_FULL,
	RING_BUFFER_NO_SUFFICIENT_SPACE
} RingBuffer_Status;

uint16_t RingBuffer_GetDataLength(RingBuffer *buf);
uint16_t RingBuffer_GetFreeSpace(RingBuffer *buf);
extern void RingBuffer_Init(RingBuffer *buf);
uint16_t RingBufferReadTrash(RingBuffer *buf, float *data, uint16_t len);
uint16_t RingBufferReadForFFT(RingBuffer *buf, float *data, uint16_t len);
uint8_t RingBuffer_Write(RingBuffer *buf, uint16_t *data, uint16_t len);

#endif //#ifndef AUDIO_RING_BUFFER_H__
