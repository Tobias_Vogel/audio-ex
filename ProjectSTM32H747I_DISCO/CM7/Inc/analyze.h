/**
  ******************************************************************************
  * @file    analyze.h
  * @author  Tobias Vogel @ OBLAMATIK
  * @brief   This module implements all sound relevant analyzing function.
  * @brief   Header for analyze.c module.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 Oblamatik AG.
  * All rights reserved.</center></h2>
  *
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __ANALYZE_H
#define __ANALYZE_H

/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_hal.h"
#include "stm32h747i_discovery_audio.h"
#include "stm32h747i_discovery_lcd.h"
#include "stm32_lcd.h"

/* Exported types ------------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void ProcessSoundDisplay(void);
extern void Display_Wave(void);
extern void UpdateRecordBufferChannelLeft(uint16_t * RecBuf);
extern void Display_Sound(uint16_t * RecBuf, uint32_t ui32CurPos);
extern void Display_Wave(void);
extern void CalculateFFT(float * RecBuf);

#endif /* __ANALYZE_H */

/************************ (C) Oblamatik AG *****END OF FILE****/
